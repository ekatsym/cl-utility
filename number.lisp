(in-package :util)

(deftype positive ()
  '(not non-positive))

(deftype non-positive ()
  '(real * 0))

(deftype negative ()
  '(not non-negative))

(deftype non-negative ()
  '(real 0 *))

(deftype natural ()
  '(integer 0 *))

(deftype index ()
  '(and non-negative fixnum))
