(defsystem "util"
  :version "0.1.0"
  :author "Hirotetsu Hongo"
  :license "LLGPL"
  :components (;; general
               (:file "package")
               (:file "number")
               (:file "list")
               (:file "symbol")
               (:file "string")
               (:file "sequence")
               (:file "function")
               (:file "defmacro")
               (:file "macro")
               (:file "io")
               (:file "array")
               (:file "multiple-value")
               (:file "sort")
               (:file "anaphora")
               (:file "dispatch")
               (:file "nondeterminism")

               ;; specific
               (:file "alist")
               (:file "closure")
               (:file "lazy")
               (:file "continuation")
               ;(:file "match")
               ;(:file "data")
               ;(:file "monad")
               ))
