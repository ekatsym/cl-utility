(in-package :util)

(defun macroexpand-all (form &optional env)
  (declare (ignorable env))

  #+abcl
  (values (ext:macroexpand-all form env) t t)

  #+allegro
  #+(and allegro-version>= (version>= 8 0))
  (values (excl::walk-form form) t nil)
  #-(and allegro-version>= (version>= 8 0))
  (values (excl::walk form) t nil)

  #+ccl
  (values (ccl:macroexpand-all form env) t t)

  #+clisp
  (values (ext:expand-form form) t nil)

  #+cmucl
  (values (walker:macroexpand-all form env) t t)

  #+corman
  (values (ccl:macroexpand-all form) t nil)

  #+ecl
  (values (walker:macroexpand-all form env) t t)

  #+lispworks
  (values (walker:walk-form form) t nil)

  #+mkcl
  (values (walker:macroexpand-all form) t nil)

  #+sbcl
  (values (sb-cltl2:macroexpand-all form env) t t)

  #+scl
  (values (macroexpand form) t nil)

  #-(or abcl allegro ccl clisp cmucl corman ecl lispworks mkcl sbcl scl)
  (values form nil nil))

(defun code-walker (fatom fapp expr env)
  (let ((expanded-expr (macroexpand-all expr env)))
    (if (atom expr)
        (atom-walker fatom fapp expr env)
        (cons-walker fatom fapp expr env))))

(defun atom-walker (fatom fapp expr env)
  (funcall fatom expr))

(defun cons-walker (fatom fapp expr env)
  (destructuring-bind (op &rest args) expr
    (if (atom op)
        (if (special-operator? op)
            (special-form-walker fatom fapp op args env)
            (application-walker fatom fapp op args env))
        (application-walker fatom fapp 'funcall expr env))))

(defun application-walker (fatom fapp op args env)
  (code-walker fatom fapp (apply fapp op args) env))

(defun special-operator? (atom)
  (inq atom
       block catch eval-when flet function go if labels let let*
       load-time-value locally macrolet multiple-value-call
       multiple-value-prog1 progn progv quote return-from
       setq symbol-macrolet tagbody the throw unwind-protect))

(defun special-form-walker (fatom fapp op args env)
  (switch (op :test #'eq)
    (block (block-walker fatom fapp args env))
    (catch (catch-walker fatom fapp args env))
    (eval-when (eval-when-walker fatom fapp args env))
    (flet (flet-walker fatom fapp args env))
    (function (function-walker fatom fapp args env))
    (go (go-walker fatom fapp args env))
    (if (if-walker fatom fapp args env))
    (labels (labels-walker fatom fapp args env))
    (let (let-walker fatom fapp args env))
    (let* (let*-walker fatom fapp args env))
    (load-time-value (load-time-value-walker fatom fapp args env))
    (locally (locally-walker fatom fapp args env))
    (macrolet (macrolet-walker fatom fapp args env))
    (multiple-value-call (multiple-value-call-walker fatom fapp args env))
    (multiple-value-prog1 (multiple-value-prog1-walker fatom fapp args env))
    (progn (progn-walker fatom fapp args env))
    (progv (progv-walker fatom fapp args env))
    (quote (quote-walker fatom fapp args env))
    (return-from (return-from-walker fatom fapp args env))
    (setq (setq-walker fatom fapp args env))
    (symbol-macrolet (symbol-macrolet-walker fatom fapp args env))
    (tagbody (tagbody-walker fatom fapp args env))
    (the (the-walker fatom fapp args env))
    (throw (throw-walker fatom fapp args env))
    (unwind-protect (unwind-protect-walker fatom fapp args env))))

(defun shadowing-atom (fatom atoms)
  (lambda (a) (if (member a atoms) a (funcall fatom a))))

(defun shadowing-app (fapp ops)
  (lambda (o &rest as) (if (member o ops) (cons o as) (apply fapp o as))))

(defun block-walker (fatom fapp args env)
  (destructuring-bind (name &rest forms) args
    `(block ,name ,@(mapcar (lambda (form) (code-walker fatom fapp form env)) forms))))

(defun catch-walker (fatom fapp args env)
  (destructuring-bind (tag &rest body) args
    `(catch ,tag ,@(mapcar (lambda (expr) (code-walker fatom fapp expr env)) body))))

(defun eval-when-walker (fatom fapp args env)
  (destructuring-bind (situations &rest forms) args
    `(eval-when ,situations ,@(mapcar (lambda (form) (code-walker fatom fapp form env)) forms))))

(defun flet-walker (fatom fapp args env)
  (destructuring-bind (definitions &rest body) args
    `(flet ,(mapcar (lambda (definition)
                      (destructuring-bind (name args &rest body) definition
                        `(,name ,args ,@(mapcar (lambda (expr) (code-walker fatom fapp expr env)) body))))
                    definitions)
       ,@(mapcar (lambda (expr)
                   (code-walker
                     fatom
                     (shadowing-app fapp (mapcar #'first definitiions))
                     expr
                     env))
                 body))))

(defun function-walker (fatom fapp args env)
  `(function ,@args))

(defun go-walker (fatom fapp args env)
  `(go ,@args))

(defun if-walker (fatom fapp args env)
  (destructuring-bind (test then &optional else) args
    `(if ,(code-walker fatom fapp test env)
         ,(code-walker fatom fapp then env)
         ,(code-walker fatom fapp else env))))

(defun labels-walker (fatom fapp args env)
  (destructuring-bind (definitions &rest body) args
    `(labels ,(mapcar (lambda (definition)
                        (destructuring-bind (name args &rest body) definition
                          `(,name ,args ,@(mapcar (lambda (expr) (code-walker fatom fapp expr env)) body))))
                      definitions)
       ,@(mapcar (lambda (expr) (code-walker fatom fapp expr env)) body))))

(defun let-walker (fatom fapp args env)
  (destructuring-bind (bindings &rest body) args
    `(let ,(mapcar (lambda (binding)
                     (destructuring-bind (name &rest body) binding
                       `(,name ,@(mapcar (lambda (expr) (code-walker fatom fapp expr env)) body))))
                   bindings)
       ,@(mapcar (lambda (expr) (code-walker fatom fapp expr env)) body))))

(defun let*-walker (fatom fapp args env)
  (destructuring-bind (bindings &rest body) args
    `(let* ,(mapcar (lambda (binding)
                      (destructuring-bind (name &rest body) binding
                        `(,name ,@(mapcar (lambda (expr) (code-walker fatom fapp expr env)) body))))
                    bindings)
       ,@(mapcar (lambda (expr) (code-walker fatom fapp expr env)) body))))

(defun load-time-value-walker (fatom fapp args env)
  (destructuring-bind (form &optional read-only-p) args
    `(load-time-value ,(code-walker fatom fapp form env) ,(code-walker fatom fapp form env))))

(defun locally-walker (fatom fapp args env)
  `(locally ,@(mapcar (lambda (expr) (code-walker fatom fapp expr env)) args)))

(defun macrolet-walker (fatom fapp args env)
  (destructuring-bind (definitions &rest body) args
    `(macrolet ,(mapcar (lambda (definition)
                          (destructuring-bind (name args &rest body) definition
                            `(,name ,args ,@(mapcar (lambda (expr) (code-walker fatom fapp expr env)) body))))
                        definitions)
       ,@body)))
