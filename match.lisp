(in-package :util)

(defmacro! match-list (o!form &body cases)
  (let ((lengths (remove-duplicates (mapcar (compose #'length #'first) cases))))
    `(case ,g!form
       ,@(mapcar (lambda (len)
                   `(,len ,@(%match-list g!form
                                         (remove-if-not (compose (partial #'= len) #'length #'first)
                                                        cases))))
                 lengths))))


(defun %match-list (expression pattern body)
  (cond ((endp pattern)
         `((endp ,expression) ,@body))
        ((symbolp pattern)
         
         )
        )
  )

(match-list (list 0 1 2)
  ((a b) (values a b))
  ((0 a b) (values a b))
  ((a b c) (values a b c))
  )

(defun %case-transform (form case)
  (cons-let (((test then) case))
    `((and (n-list? ,(length test) ,form)
           ,@(mapcar (lambda (i x)
                       (cond ((symbolp x) ))
                       )
                     (enumerate (length test) )
                     )
           )
      (let ,(mapcar (lambda (x) (if (symbolp x)
                                    ()
                                    ))
             ))
      )
    )
  )


