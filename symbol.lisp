(in-package :util)

(defun symbolicate (&rest things)
  (identity
    (intern (apply #'concatenate
                   'string
                   (mapcar #'string things)))))

(defun gensyms (n)
  (labels ((rec (i acc)
             (declare (optimize (speed 3))
                      (type index i))
             (if (zerop i)
                 (nreverse acc)
                 (rec (1- i) (cons (gensym) acc)))))
    (rec n '())))

(defun explode (symbol)
  (map 'list
       (lambda (c) (intern (make-string 1 :initial-element c)))
       (symbol-name symbol)))
