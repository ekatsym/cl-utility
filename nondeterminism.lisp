(in-package :util)

(defmacro! nondeterministic-let (bindings &body body)
  (let ((gs (gensyms (length bindings)))
        (vars (mapcar #'first bindings))
        (lst (mapcar #'rest bindings)))
    `(nondeterministic-let*
       ,(mapcar (lambda (g l) `(,g ,@l))
                gs lst)
       (let ,(mapcar #'list vars gs) ,@body))))

(defmacro! nondeterministic-let* (bindings &body body)
  (labels ((rec (bindings body)
             (if (endp bindings)
                 `(pushnew (progn ,@body) ,g!result)
                 (destructuring-bind ((var lst) &rest rest-bindings) bindings
                   `(dolist (,var ,lst)
                      ,(rec rest-bindings body))))))
    `(let ((,g!result '()))
       ,(rec bindings body)
       ,g!result)))

(defmacro nondeterministic-let/filter (bindings &body body)
  (let ((gs (gensyms (length bindings)))
        (vars (mapcar #'first bindings))
        (lst (mapcar #'rest bindings)))
    `(nondeterministic-let*/filter
       ,(mapcar (lambda (g l) `(,g ,@l))
                gs lst)
       (let ,(mapcar #'list vars gs) ,@body))))

(defmacro! nondeterministic-let*/filter (bindings &body body)
  (labels ((rec (bindings body)
             (if (endp bindings)
                 `(awhen (progn ,@body)
                    (pushnew it ,g!result))
                 (destructuring-bind ((var lst) &rest rest-bindings) bindings
                   `(dolist (,var ,lst)
                      ,(rec rest-bindings body))))))
    `(let ((,g!result '()))
       ,(rec bindings body)
       ,g!result)))
