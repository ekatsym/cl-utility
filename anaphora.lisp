(in-package :util)

(defmacro alet (form &body body)
  `(let ((it ,form))
     ,@body))

(defmacro! aif (o!test then &optional else)
  `(if ,g!test
       (let ((it ,g!test))
         ,then)
       ,else))

(defmacro acond (&rest clauses)
  (if (null clauses)
      'nil
      (cons-let* (((clause rest-clauses) clauses)
                  ((test body) clause))
        `(aif ,test
              (progn ,@body)
              ,(if (null rest-clauses)
                   'nil
                   `(acond ,@rest-clauses))))))

(defmacro awhen (test &body forms)
  `(let ((it ,test))
     (when it
       ,@forms)))

(defmacro aunless (test &body forms)
  `(let ((it ,test))
     (unless it
       ,@forms)))

(defmacro aand (&rest forms)
  (cond ((null forms) t)
        ((n-list? 1 forms) (first forms))
        (t `(awhen ,(first forms)
              (aand ,@(rest forms))))))

(defmacro acase (keyform &body cases)
  `(let ((it ,keyform))
     (case it
       ,@cases)))

(defmacro aecase (keyform &body cases)
  `(let ((it ,keyform))
     (ecase it ,@cases)))

(defmacro accase (keyform &body cases)
  `(let ((it ,keyform))
     (ccase it ,@cases)))

(defmacro atypecase (keyform &body cases)
  `(let ((it ,keyform))
     (typecase it ,@cases)))

(defmacro aetypecase (keyform &body cases)
  `(let ((it ,keyform))
     (etypecase it ,@cases)))

(defmacro actypecase (keyform &body cases)
  `(let ((it ,keyform))
     (ctypecase it ,@cases)))

(defmacro alambda (args &body body)
  `(labels ((self ,args ,@body))
     #'self))
