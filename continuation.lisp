(in-package :util)

(defmacro continuous (var receiver &body body)
  `(funcall ,receiver (lambda (,var) (declare (ignorable ,var)) ,@body)))

(defmacro multiple-value-continuous (var receiver &body body)
  `(funcall ,receiver (lambda (&rest ,var) (declare (ignorable ,var)) ,@(subst `(apply #'values ,var) var body))))
