(in-package :util)

(defun alist? (object)
  (declare (optimize (speed 3)))
  (or (null object)
      (and (consp object)
           (consp (car object))
           (alist? (cdr object)))))

(deftype alist ()
  '(satisfies alist?))

(defun alist (&rest args)
  (labels ((rec (lst acc)
             (declare (optimize (speed 3)))
             (if (endp lst)
                 acc
                 (rec (cddr lst) (acons (car lst) (cadr lst) acc)))))
    (rec args '())))

(defun aget (key alist &key (test #'eql))
  "Finds the entry in ALIST whose key is KEY
   and returns an associated value as first value of multiple value.
   Second value of multiple value means wheather the entry exists or does not."
  (declare (optimize (speed 3))
           (type function test))
  (cond ((endp alist)
         (values nil nil))
        ((funcall test key (caar alist))
         (values (cdar alist) t))
        (t
         (aget key (cdr alist) :test test))))

(defun aset (key value alist &key (test #'eql))
  (declare (type function test))
  (labels ((rec (alst acc)
             (declare (optimize (speed 3)))
             (if (endp alst)
                 (acons key value acc)
                 (destructuring-bind ((k . v) &rest rest) alst
                   (if (funcall test k key)
                       (revappend acc (acons key value rest))
                       (rec rest (acons k v acc)))))))
    (rec alist '())))

(defun arem (key alist &key (test #'eql))
  (remove-if (lambda (pair) (funcall test key (car pair)))
             alist))

(defun amap (function alist)
  (declare (type function function))
  (labels ((rec (alst acc)
             (declare (optimize (speed 3)))
             (if (endp alst)
                 (nreverse acc)
                 (rec (cdr alst)
                      (acons (caar alst)
                             (funcall function (cdar alst))
                             acc)))))
    (rec alist '())))
