(in-package :util)

(defun compose (&rest functions)
  (labels ((rec (fns acc)
             (declare (optimize (speed 3))
                      (type function acc))
             (if (endp fns)
                 acc
                 (rec (rest fns)
                      (lambda (&rest args)
                        (funcall acc (apply (the function (first fns)) args)))))))
    (if functions
        (rec (rest functions) (first functions))
        #'values)))

(defun ncompose (function n)
  (declare (type function function)
           (type index n))
  (labels ((rec (i acc)
             (declare (type index i))
             (if (zerop i)
                 acc
                 (rec (1- i)
                      (lambda (x)
                        (funcall
                          function (funcall
                                     acc x)))))))
    (if (zerop n)
        #'values
        (rec (1- n) function))))

(defun memoize (function)
  (let ((cache (make-hash-table :test #'equal)))
    (lambda (&rest args)
      (multiple-value-bind (value cache?) (gethash args cache)
        (if cache?
            value
            (setf (gethash args cache)
                  (apply function args)))))))

(defun partial (function &rest args)
  (lambda (&rest rest-args) (apply function (append args rest-args))))

(defun rpartial (function &rest args)
  (lambda (&rest rest-args) (apply function (append rest-args args))))

(defun curry (function)
  (lambda (&rest args)
    (lambda (&rest rest-args)
      (apply function (append args rest-args)))))

(defun rcurry (function)
  (lambda (&rest args)
    (lambda (&rest rest-args)
      (apply function (reverse (append args rest-args))))))

(defun pipe (arg &rest functions)
  (declare (optimize (speed 3)))
  (if (null functions)
      arg
      (apply #'pipe (funcall (the function (first functions)) arg) (rest functions))))

(defun fn-if (test then else)
  (lambda (x)
    (if (funcall test x)
        (funcall then x)
        (funcall else x))))

(defun fn-and (&rest functions)
  (cond ((null functions)
         (lambda (&rest args) t))
        ((n-list? 1 functions)
         (first functions))
        (t
         (let ((chain (apply #'fn-and (rest functions))))
           (lambda (&rest args)
             (and (apply (first functions) args)
                  (apply chain args)))))))

(defun fn-or (&rest functions)
  (cond ((null functions)
         (lambda (&rest args) nil))
        ((n-list? 1 functions)
         (first functions))
        (t
         (let ((chain (apply #'fn-or (rest functions))))
           (lambda (&rest args)
             (or (apply (first functions) args)
                 (apply chain args)))))))

(defun recursion (base? base-case step-back rec-step)
  (labels ((self (x)
             (if (funcall base? x)
                 base-case
                 (funcall rec-step x (self (funcall step-back x))))))
    #'self))

(defun tail-recursion (base? base-case step-back rec-step &optional (finally #'identity))
  (declare (type function base? step-back rec-step finally))
  (lambda (x) (do ((i x (funcall step-back i))
                   (acc base-case (funcall rec-step i acc)))
                  ((funcall base? i)
                   (funcall finally acc)))))

(defun tree-traverse (rec-step &optional (base-case #'identity))
  (labels ((self (tree)
             (if (atom tree)
                 (if (functionp base-case)
                     (funcall base-case tree)
                     base-case)
                 (funcall rec-step
                          (self (car tree))
                          (when (cdr tree)
                            (self (cdr tree)))))))
    #'self))

(defun tree-recursion (rec-step &optional (base-case #'identity))
  (labels ((self (tree)
             (if (atom tree)
                 (if (functionp base-case)
                     (funcall base-case tree)
                     base-case)
                 (funcall rec-step tree
                          (lambda () (self (car tree)))
                          (lambda () (when (cdr tree)
                                       (self (cdr tree))))))))
    #'self))
