(in-package :util)

(defgeneric ret (type x))
(defgeneric bind (monad function))

(deftype maybe (&optional (type '*))
  `(or (eql nothing)
       (cons (eql just) (cons ,type null))))

(defun just (x)
  (list 'just x))

(defmethod ret ((type (eql 'maybe)) x)
  (just x))

(defmethod bind ((m maybe)))

(typep '(just 10) '(maybe number))
