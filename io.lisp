(in-package :util)

(defun read-list (&optional (stream *standard-input*) (eof-error-p t) eof-value recursive-p)
  (identity
    (read-from-string
      (concatenate 'string "(" (read-line stream eof-error-p eof-value recursive-p) ")"))))

(defmacro! do-line-stream ((var o!stream &optional (result nil)) &body body)
  `(do ((,var (read-line ,g!stream nil ',g!eof) (read-line ,g!stream nil ',g!eof)))
       ((eq ,var ',g!eof) ,result)
       ,@body))

(defmacro! do-char-stream ((var o!stream &optional (result nil)) &body body)
  `(do ((,var (read-char ,g!stream nil ',g!eof) (read-char ,g!stream nil ',g!eof)))
       ((eq ,var ',g!eof) ,result)
       ,@body))

(defun read-stream-into-string (stream)
  (with-output-to-string (out)
    (do-char-stream (c stream)
      (princ c out))))

(defun read-file-into-string (filespec)
  (with-open-file (s filespec)
    (read-stream s)))

(defun read-stream-into-byte-array (binary-input-stream)
  (let* ((len (file-length binary-input-stream))
         (arr (make-array len :element-type '(unsigned-byte 8))))
    (dotimes (i len arr)
      (setf (aref arr i) (read-byte binary-input-stream)))))

(defun read-binary-file-into-byte-array (filespec)
  (with-open-file (s filespec :element-type '(unsigned-byte 8))
    (binary-input-stream->byte-array s)))

(defun prompt (control-string &rest format-arguments)
  (apply #'format *query-io* control-string format-arguments)
  (read *query-io*))

(defun break-loop (eval quit control-string &rest format-arguments)
  (format *query-io* "Entering breaking-loop.~%")
  (loop
    (let ((in (apply #'prompt control-string format-arguments)))
      (if (funcall quit in)
          (return)
          (format *query-io* "~a~%" (funcall eval in))))))
