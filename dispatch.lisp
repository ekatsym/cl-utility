(in-package :util)

(defmacro! dlambda (&rest clauses)
  `(lambda (,g!key &rest ,g!args)
     (ecase ,g!key
       ,@(mapcar (lambda (clause)
                   (destructuring-bind (key args . body) clause
                     `(,key (apply (lambda ,args ,@body) ,g!args))))
                 clauses))))

(defmacro! ddefun (name &body clauses)
  `(defun ,name (,g!key &rest ,g!args)
     (ecase ,g!key
       ,@(mapcar (lambda (clause)
                   (destructuring-bind (key args . body) clause
                     `(,key (apply (lambda ,args ,@body) ,g!args))))
                 clauses))))

(defmacro! dflet (definitions &body body)
  `(flet ,(mapcar (lambda (definition)
                    (destructuring-bind (name . clauses) definition
                      `(,name (,g!key &rest ,g!args)
                              (ecase ,g!key
                                ,@(mapcar (lambda (clause)
                                            (destructuring-bind (key args . body) clause
                                              `(,key (apply (lambda ,args ,@body) ,g!args))))
                                          clauses)))))
                  definitions)
     ,@body))

(defmacro! dlabels (definitions &body body)
  `(labels ,(mapcar (lambda (definition)
                      (destructuring-bind (name . clauses) definition
                        `(,name (,g!key &rest ,g!args)
                                (ecase ,g!key
                                  ,@(mapcar (lambda (clause)
                                              (destructuring-bind (key args . body) clause
                                                `(,key (apply (lambda ,args ,@body) ,g!args))))
                                            clauses)))))
                    definitions)
     ,@body))
