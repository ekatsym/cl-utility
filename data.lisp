(in-package :util)

(defmacro defdata (name &body constructors)
  `(progn
     (deftype ,name ()
       '(or ,@(mapcar (lambda (c)
                        (if (atom c)
                            c
                            (first c)))
                      constructors)))
     ,@(mapcar (lambda (c)
                 (if (atom c)
                     `(deftype ,c ()
                        '(eql ,c))
                     `(progn
                        (deftype ,(first c) ()
                          '(satisfies ,(symbolicate (first c) '?)))
                        (defun ,(symbolicate (first c) '?) (object)
                          (and (consp object)
                               (eq (car object) ',(first c))
                               (length= object ',c)
                               ,@(mapcar (lambda (i x)
                                           `(typep (nth ,i object) ',x))
                                         (enumerate (length (rest c)) :from 1)
                                         (rest c)))))))
               constructors)
     ,@(mapcar (lambda (c)
                 (if (atom c)
                     `(defparameter ,c ',c)
                     (let ((cname (first c))
                           (cargs (mapcar (compose #'gensym #'string) (rest c))))
                       `(defun ,cname ,cargs
                          (list ',cname ,@cargs)))))
               constructors)
     ',name))

(defmacro! match-data (o!form &body clauses)
  `(cond ,@(mapcar (lambda (clause)
                     (destructuring-bind (match . body) clause
                       (if (atom match)
                           (if (inq match t otherwise)
                               `(t ,@body)
                               `((typep ,g!form ',match) ,@body))
                           `((typep ,g!form ',(first match))
                             (let ,(mapcar (lambda (x)
                                             (if (atom x)
                                                 `()
                                                 )
                                             )
                                           ,(rest match))
                               ,@body)))))
                   clauses)
         (t (error "~2t~a~%fell through MATCH-DATA." ,g!form))))

(defdata maybe
  nothing
  (just t))

(match-data (just 10)
  (nothing nil)
  ((just a) a))

(defdata my-list
  my-nil
  (my-cons t my-list))

(match-data (my-cons 0 (my-cons 1 (my-cons 2 my-nil)))
  ((my-cons a b) (list a b))
  (my-nil 100))
